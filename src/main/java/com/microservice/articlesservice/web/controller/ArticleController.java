package com.microservice.articlesservice.web.controller;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.microservice.articlesservice.dao.ArticleDao;
import com.microservice.articlesservice.model.Article;
import com.microservice.articlesservice.web.exceptions.NotFoundArticleException;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@Api(description = "Api pour les opérations CRUD")
public class ArticleController {

    private static final Logger logger = LoggerFactory.getLogger(ArticleController.class);


    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private HttpServletRequest requestContext ;


    @GetMapping(value = "/Articles")
    public MappingJacksonValue listArticles() {

        logger.info("Début d'appel au service Articles pour la requête : " + requestContext.getHeader("req-id"));

        List<Article> articles = articleDao.findAll();

        MappingJacksonValue filteredData = getFilteredDataFromArray(articles, "filtreMarge", new String[] { "marge", "prixAchat" });

        return filteredData;
    }

    @GetMapping(value = "/Articles/{id}")
    public MappingJacksonValue getArticleById(@PathVariable int id) {

        Article article  = articleDao.findById(id);

        if (article == null) {
            throw new NotFoundArticleException("Article " + id + " not found");
        }

        MappingJacksonValue filteredData = getFilteredDataObject(article, "filtreMarge", new String[] { "marge", "prixAchat" });

        return filteredData;
    }

    @GetMapping(value = "/Articles/price/{priceLimit}")
    public MappingJacksonValue getArticlesGreater(@PathVariable int priceLimit) {

        List<Article> articles = articleDao.findByPrixGreaterThan(priceLimit);
        MappingJacksonValue filteredData = getFilteredDataFromArray(articles, "filtreMarge", new String[] { "marge", "prixAchat" });

        return filteredData;
    }

    @GetMapping(value = "/Articles/search/{word}")
    public MappingJacksonValue searchArticles(@PathVariable String word) {

        List<Article> articles = articleDao.findByNomLike("%" + word + "%");
        MappingJacksonValue filteredData = getFilteredDataFromArray(articles, "filtreMarge", new String[] { "marge", "prixAchat" });

        return filteredData;
    }

    @GetMapping(value = "/AdminArticles")
    public MappingJacksonValue getMargeOfAllArticles() {

        List<Article> articles = articleDao.findAll();
        MappingJacksonValue filteredData = getFilteredDataFromArray(articles, "filtreMarge", new String[] {});

        return filteredData;
    }

    @GetMapping(value = "/Articles/sort")
    public MappingJacksonValue getAllSortByAlp() {

        List<Article> articles = articleDao.findByOrderByNomAsc();
        MappingJacksonValue filteredData = getFilteredDataFromArray(articles, "filtreMarge", new String[] { "marge", "prixAchat" });

        return filteredData;
    }

    @PostMapping(value = "/Articles")
    public ResponseEntity<Void> addArticle(@RequestBody Article article) {

        Article articleAdded = articleDao.save(article);

        if (articleAdded == null) {
            return ResponseEntity.noContent().build();
        } else {
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(articleAdded.getId()).toUri();
            return ResponseEntity.created(location).build();
        }
    }

    @PutMapping(value = "/Articles")
    public void updateArticle(@RequestBody Article article) {
        articleDao.save(article);
    }

    @DeleteMapping(value = "/Articles/{id}")
    public void deleteArticleById(@PathVariable int id) {
        articleDao.deleteById(id);
    }

    private MappingJacksonValue getFilteredDataFromArray (List<Article> articles, String filter, String[] properties) {

        SimpleBeanPropertyFilter myFilter = SimpleBeanPropertyFilter.serializeAllExcept(properties);
        FilterProvider listOfMyFilters = new SimpleFilterProvider().addFilter(filter, myFilter);
        MappingJacksonValue filterArticles = new MappingJacksonValue(articles);

        filterArticles.setFilters(listOfMyFilters);

        return filterArticles;
    }

    private MappingJacksonValue getFilteredDataObject(Article article, String filter, String[] properties) {

        SimpleBeanPropertyFilter myFilter = SimpleBeanPropertyFilter.serializeAllExcept(properties);
        FilterProvider listOfMyFilters = new SimpleFilterProvider().addFilter(filter, myFilter);
        MappingJacksonValue filterArticles = new MappingJacksonValue(article);

        filterArticles.setFilters(listOfMyFilters);

        return filterArticles;
    }


}
