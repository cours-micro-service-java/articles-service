package com.microservice.articlesservice.model;

import com.fasterxml.jackson.annotation.JsonFilter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@JsonFilter("filtreMarge")
@Entity
public class Article {

    @Id
    private int id;
    private String nom;
    private int prix;
    private int prixAchat;
    @Transient
    private int marge;

    public Article() {}

    public Article(int id, String nom, int prix, int prixAchat) {
        this.id = id;
        this.nom = nom;
        this.prix = prix;
        this.prixAchat = prixAchat;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public int getPrix() {
        return prix;
    }

    public int getPrixAchat() {
        return prixAchat;
    }

    public int getMarge() {
        return prix - prixAchat;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public void setPrixAchat(int prixAchat) {
        this.prixAchat = prixAchat;
    }
}
